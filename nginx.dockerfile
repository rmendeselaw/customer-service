FROM nginx:latest
MAINTAINER Rozenildo Mendes
COPY /config/nginx.conf /etc/nginx/nginx.conf
EXPOSE 80 443
ENTRYPOINT ["nginx"]
CMD ["-g", "daemon off;"]
