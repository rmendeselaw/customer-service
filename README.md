# Desafio Builder

Para a construção da REST API considerei que seria um serviço utilizado por diversos clientes, o próximo passo foi iniciar a construção do modelo e camadas do serviço. Confesso que não utilizei o TDD inicialmente, mas construí testes para ações de recuperar e salvar um recurso. 

Toda a infraestrutura está disponível em container e utilizando o docker compose para orquestração, isso possibilitou escalar o serviço não mantendo um único ponto de falha, e para o balanceamento de carga o nginx foi essencial.

Na raiz do projeto se encontra o arquivo postman customer-service.json onde contém os testes do serviço.

Endpoint localizado em: http://34.121.207.202/customers
Documentação localizada em: http://34.121.207.202/swagger-ui.html


