package io.builders.customer.base;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import io.builders.model.Customer;
import io.builders.repository.CustomerRepository;
import io.builders.resource.CustomerResource;
import io.restassured.module.mockmvc.RestAssuredMockMvc;

@ImportAutoConfiguration(exclude=HibernateJpaAutoConfiguration.class)
@SpringBootTest
@RunWith(SpringRunner.class)
public class CustomersBase {

	@Autowired
	private CustomerResource customerResource;

	@MockBean
	private CustomerRepository customerRepository;

	@Before
	public void before() {
		RestAssuredMockMvc.standaloneSetup(customerResource);
		Mockito.when(customerRepository.save(Mockito.any(Customer.class)))
				.thenAnswer((InvocationOnMock invocation) -> {
					Customer customer = invocation.getArgument(0);
					return customer;
				});
	}

}
