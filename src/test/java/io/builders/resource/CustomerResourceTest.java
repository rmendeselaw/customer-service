package io.builders.resource;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import io.builders.model.Address;
import io.builders.model.Customer;
import io.builders.resource.input.CustomerInput;
import io.builders.resource.ouput.CustomerOutput;
import io.builders.service.CustomerService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = CustomerResource.class)
public class CustomerResourceTest {
	
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private CustomerService customerService;
	
	@Test
	public void listById() throws Exception {

		Customer customerMock = new Customer();
		customerMock.setId(1L);
		customerMock.setName("Rozenildo Mendes");
		customerMock.setEmail("rmendeseti@gmail.com");
		customerMock.setBirthDate(LocalDate.of(1990, 8, 19));
		
		Address address = new Address();
		address.setAddress("Av São Bento");
		address.setCity("Sorriso");
		address.setState("MT");
		address.setZip("78890000");
		
		customerMock.setAddress(address);
		
		Mockito.when(
				customerService.findById(Mockito.anyLong())).thenReturn(customerMock);

		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.get("/customers/1")
				.accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		String expected = "{\"id\":1,\"name\":\"Rozenildo Mendes\",\"birthDate\":\"1990-08-19\",\"email\":\"rmendeseti@gmail.com\",\"address\":\"Av São Bento\",\"city\":\"Sorriso\",\"state\":\"MT\",\"zip\":\"78890000\",\"age\":30}";

		Assert.assertEquals(expected, result.getResponse().getContentAsString());
				
	}
	
	@Test
	public void create() throws Exception {
		
		CustomerOutput customerOutput = new CustomerOutput();
		customerOutput.setId(1L);
		
		Mockito
			.when(customerService.create(Mockito.any(CustomerInput.class)))
			.thenReturn(customerOutput);

		String payload = "{\"name\":\"Rozenildo Mendes\",\"birthDate\":\"1990-08-19\",\"email\":\"rmendeseti@gmail.com\",\"address\":\"Av São Bento\",\"city\":\"Sorriso\",\"state\":\"MT\",\"zip\":\"78890000\",\"age\":30}";
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/customers")
				.accept(MediaType.APPLICATION_JSON).content(payload)
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		MockHttpServletResponse response = result.getResponse();

		assertEquals(HttpStatus.CREATED.value(), response.getStatus());
		assertEquals("http://localhost/customers/1", response.getHeader(HttpHeaders.LOCATION));

	}

}
