import org.springframework.cloud.contract.spec.Contract
Contract.make {
    description "deve adicionar novo cliente"
    request{
        method POST()
        url("/customers")
        body([
            name: 'Rozenildo Mendes',
			birthDate: '1990-08-19',
			email: 'rmendeseti@gmail.com',
			address: 'Av São Bento',
			city: 'Sorriso',
			state: 'MT',
			zip: '78890000'
        ])
        headers {
            contentType('application/json')
        }
    }
    response {
        status 201
        body([
            id: 1,
            name: 'Rozenildo Mendes',
			birthDate: '1990-08-19',
			email: 'rmendeseti@gmail.com',
			address: 'Av São Bento',
			city: 'Sorriso',
			state: 'MT',
			zip: '78890000'
        ])
        headers {
            contentType('application/json')
        }
    }
}