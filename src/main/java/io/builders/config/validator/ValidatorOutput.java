package io.builders.config.validator;

public class ValidatorOutput {
	
	private String field;
	private String error;
	
	public ValidatorOutput(String field, String error) {
		this.field = field;
		this.error = error;
	}

	public String getField() {
		return field;
	}

	public String getError() {
		return error;
	}

}