package io.builders.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import io.builders.model.Customer;
import io.builders.resource.input.Emailnput;
import io.builders.resource.input.CustomerInput;
import io.builders.resource.ouput.CustomerOutput;

public interface CustomerService {

	Page<CustomerOutput> findAll(Pageable page);

	Customer findById(Long id);

	CustomerOutput create(CustomerInput customerInput);

	CustomerOutput update(Long id, CustomerInput customerInput);

	void updateEmail(Long id, Emailnput email);

}
