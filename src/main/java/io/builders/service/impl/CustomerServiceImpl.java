package io.builders.service.impl;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import io.builders.model.Customer;
import io.builders.repository.CustomerRepository;
import io.builders.resource.input.CustomerInput;
import io.builders.resource.input.Emailnput;
import io.builders.resource.ouput.CustomerOutput;
import io.builders.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {
	
	private final CustomerRepository customerRepository;
	
	public CustomerServiceImpl(CustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
	}

	@Override
	public Page<CustomerOutput> findAll(Pageable page) {
		
		return customerRepository.findAll(page)
				.map(customer -> new CustomerOutput(customer));
	}

	@Override
	public Customer findById(Long id) {
		
		return customerRepository.findById(id)
				.orElseThrow(() -> new EmptyResultDataAccessException(1));
	}

	@Override
	public CustomerOutput create(CustomerInput customerInput) {
		
		Customer customer = customerInput.parseInstance();
		return new CustomerOutput(customerRepository.save(customer));
	}

	@Override
	public CustomerOutput update(Long id, CustomerInput input) {
		
		Customer customer = this.findById(id);
		
		customer.setName(input.getName());
		customer.setEmail(input.getEmail());
		customer.setBirthDate(input.getBirthDate());
		customer.getAddress().setAddress(input.getAddress());
		customer.getAddress().setCity(input.getCity());
		customer.getAddress().setState(input.getState());
		customer.getAddress().setZip(input.getZip());
		
		return new CustomerOutput(customerRepository.save(customer));
		
	}

	@Override
	public void updateEmail(Long id, Emailnput input) {
		
		Customer customer = this.findById(id);
		customer.setEmail(input.getEmail());
		
		customerRepository.save(customer);
		
	}

}
