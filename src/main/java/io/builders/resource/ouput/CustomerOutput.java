package io.builders.resource.ouput;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import io.builders.model.Customer;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CustomerOutput {
	
	private Long id;
	private String name;
	private LocalDate birthDate;
	private String email;
	private String address;
	private String city;
	private String state;
	private String zip;
	private long age;
	
	public CustomerOutput(Customer customer) {
		
		id = customer.getId();
		name = customer.getName();
		birthDate = customer.getBirthDate();
		email = customer.getEmail();
		address = customer.getAddress().getAddress();
		city = customer.getAddress().getCity();
		state = customer.getAddress().getState();
		zip = customer.getAddress().getZip();
		age = ChronoUnit.YEARS.between(customer.getBirthDate(), LocalDate.now());
				
	}

}
