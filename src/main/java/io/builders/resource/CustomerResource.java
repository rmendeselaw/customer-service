package io.builders.resource;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import io.builders.model.Customer;
import io.builders.resource.input.CustomerInput;
import io.builders.resource.input.Emailnput;
import io.builders.resource.ouput.CustomerOutput;
import io.builders.service.CustomerService;

@RestController
@RequestMapping("/customers")
public class CustomerResource {
	
	private final CustomerService customerService;
	
	public CustomerResource(CustomerService customerService) {
		this.customerService = customerService;
	}
	
	@GetMapping
	public Page<CustomerOutput> listAll(Pageable page) {
		return customerService.findAll(page);
	}
	
	@GetMapping("/{id}")
	public CustomerOutput listById(@PathVariable Long id) {
		
		Customer customer = customerService.findById(id);
		return new CustomerOutput(customer);
	}
	
	@PostMapping
	public ResponseEntity<?> create(@Valid @RequestBody CustomerInput customerInput, UriComponentsBuilder uriBuilder) {
		
		CustomerOutput customer = customerService.create(customerInput);
		
		URI location = uriBuilder.path("/customers/{id}").buildAndExpand(customer.getId()).toUri();
		return ResponseEntity.created(location).body(customer);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@PathVariable Long id, @Valid @RequestBody CustomerInput customerInput) {
		
		CustomerOutput customer = customerService.update(id, customerInput);
		return ResponseEntity.ok(customer); 
	}
	
	@PatchMapping("/{id}")
	public ResponseEntity<?> updateEmail(@PathVariable Long id, @Valid @RequestBody Emailnput input) {
		
		customerService.updateEmail(id, input);
		return ResponseEntity.ok().build();
	}

}
