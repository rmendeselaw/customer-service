package io.builders.resource.input;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import lombok.Data;

@Data
public class Emailnput {
	
	@NotEmpty @Email
	private String email;

}
