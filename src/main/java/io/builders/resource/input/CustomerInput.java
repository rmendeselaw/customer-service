package io.builders.resource.input;

import java.time.LocalDate;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import io.builders.model.Address;
import io.builders.model.Customer;
import lombok.Data;

@Data
public class CustomerInput {
	
	@NotEmpty
	private String name;
	
	@NotNull
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate birthDate;
	
	@NotEmpty @Email
	private String email;
	
	@NotEmpty
	private String address;
	
	@NotEmpty
	private String city;
	
	@NotEmpty
	private String state;
	
	@NotEmpty
	private String zip;
	
	public Customer parseInstance() {
		
		Customer customer = new Customer();
		customer.setName(name);
		customer.setBirthDate(birthDate);
		customer.setEmail(email);
		
		Address addressC = new Address();
		addressC.setAddress(address);
		addressC.setCity(city);
		addressC.setState(state);
		addressC.setZip(zip);
		
		customer.setAddress(addressC);
		
		return customer;
		
	}

}
