package io.builders.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import io.builders.model.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

}
